# bigfile-service

本项目是基于SpringBoot搭建，主要使用到的技术有：redis、redisson、s3、webflux、sentinel.

项目特点:

1.支持文件分片上传

2.秒传已经上传过的文件。

3.支持多人同时上传同一文件，可以多线程同时上传，加快了大文件的上传。

4.用Spring webflux代替Spring mvc

5.使用了alibaba的Sentinel来对接口限流

# 怎么使用?
1. 下载项目

2、配置redis和s3的信息

3、启动服务

直接运行BigfileServiceApplication 的main方法或者执行：mvn spring-boot:run

4、访问页面: http://127.0.0.1:10010/index.html
