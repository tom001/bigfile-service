package com.longge.bigfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.longge.common.annotation.EnableReactiveRedisUtilsAutoConfiguration;
import com.longge.common.annotation.EnableRedisUtilsAutoConfiguration;

@SpringBootApplication
@EnableRedisUtilsAutoConfiguration
// test redis reactive
@EnableReactiveRedisUtilsAutoConfiguration
public class BigfileServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BigfileServiceApplication.class, args);
	}

}
