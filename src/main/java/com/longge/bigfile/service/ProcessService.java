 package com.longge.bigfile.service;

import org.springframework.web.multipart.MultipartFile;

import com.longge.bigfile.common.GlobalResponse;
import com.longge.bigfile.dto.request.PostUploadRequestDto;
import com.longge.bigfile.dto.request.PreUploadRequestDto;
import com.longge.bigfile.dto.response.UploadResponseDto;

import reactor.core.publisher.Mono;

/**
 * @author roger yang
 * @date 11/05/2019
 */
public interface ProcessService {
	/**
	 *  上传前准备
	 *  主要有：
	 *  	1、生成s3/oss等的uploadId
	 *      2、redis记录关联信息
	 *      3、初始化分片信息
	 * @param dto
	 * @return
	 */
    Mono<GlobalResponse<UploadResponseDto>> preUpload(PreUploadRequestDto dto) ;

    /**
     *  上传分片信息
     * @param file
     * @param dto
     * @return
     */
    Mono<GlobalResponse<UploadResponseDto>> postUpload(MultipartFile file, PostUploadRequestDto dto) ;
}
