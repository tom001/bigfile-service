package com.longge.bigfile.util;

/**
 * @author roger yang
 * @date 11/04/2019
 */
public class FileUtils {
	private static final String SPLIT = "/";
	private static String profile;
    
    public static void setProfile(String p) {
        profile = p;
    }

    public static String getRealFilePath(String md5) {
        return profile.concat(SPLIT).concat(md5);
    }
}
