 package com.longge.bigfile.util;

import java.util.Map;

import com.aliyun.oss.OSS;
import com.longge.bigfile.config.OSSConfiguration.OSSConfig;

/**
 * @author roger yang
 * @date 12/11/2019
 */
public class OSSClientUtils {
    private static Map<String, OSS> ossClients;
    private static Map<String, OSSConfig> ossConfigs;
    
    public static void setOssClients(Map<String, OSS> clients) {
    	ossClients = clients;
    }
    
    public static void setOssConfigs(Map<String, OSSConfig> configs) {
    	ossConfigs = configs;
    }
    
    public static OSS getClient(String sys) {
    	OSS client = ossClients.get(sys);
        if(null == client) {
            throw new RuntimeException("can't find project " + sys + " oss info");
        }
        return client;
    }
    
    public static OSSConfig getConfig(String sys) {
    	OSSConfig config = ossConfigs.get(sys);
        if(null == config) {
            throw new RuntimeException("can't find project " + sys + " oss info");
        }
        return config;
    }
}
